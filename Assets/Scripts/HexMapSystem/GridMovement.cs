﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridMovement : MonoBehaviour {

    public enum Directions { NW, N, NE, SE, S, SW };
    public enum Diagonals  { SW, W, NW, NE, E, SE };

    public void Move(Directions d)
    {
        transform.Translate(GetDirectionVector(d));
    }

    public Vector3 GetDirectionVector(Directions d)
    {
        /*if (d.Equals(Directions.N)) */return new Vector3(1, 0, 0);
    }
}
