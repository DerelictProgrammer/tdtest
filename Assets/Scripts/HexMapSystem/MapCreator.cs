﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCreator : MonoBehaviour {

    GameObject lastHit; // Keep track of last object clicked
    
    void Start () {
        lastHit = null;
    }
	
	// Update is called once per frame
	void Update ()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100)) // or whatever range, if applicable
        {
            if (hit.transform.gameObject == lastHit)
            {
                // You clicked the same object twice
            }
            else
            {
                lastHit = hit.transform.gameObject;
                Debug.Log(lastHit);
            }
        }
        else
        {
            // You didn't click on anything.
            // Either out of range or empty skybox behind mouse cursor
        }
    }

    private void OnMouseUp()
    {
    }
}
