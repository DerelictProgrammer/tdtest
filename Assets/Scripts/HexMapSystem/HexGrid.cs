﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class HexGrid : MonoBehaviour {

    // Initial loaded map name
    public string mapName;

    // Object for hex (tempoary solution)
    public GameObject hexPrefab;

    // Tile radius
    public float radius = 1f;

    // Offsets for hexes
    private float offsetX, offsetY;

    // List of hex data
    private List<HexProps> hexes = null;

    // Spawned GameObjects of hexes
    private Dictionary<Coordinates.Hexagonal, Hex> spawned;
    
    [HideInInspector]
    public int CountOfHearts = 0;
    [HideInInspector]
    public int CountOfSpawners = 0;
    
    public void UpdateMapStatus()
    {
        List<string> errors = new List<string>();

        if (CountOfHearts != 1) errors.Add("Map needs exactly 1 Heart tile");
        if (CountOfSpawners != 1) errors.Add("Map needs exactly 1 Spawner tile");

        Hex spawner = GetHexOfType(Hex.HexType.SPAWNER);
        if (spawner != null)
        {
            string pathCheck = GetHexOfType(Hex.HexType.SPAWNER).CheckPath();
            if (pathCheck != null)
            {
                errors.Add(pathCheck);
            }
        }

        GameObject.FindObjectsOfType<MapErrors>().ToList().ForEach(x =>
        {
            // x.errorMessages.Clear();
            x.errorMessages = errors;
            x.DisplayErrors();
        });

        GameObject.FindObjectsOfType<StartButton>().ToList().ForEach(x =>
        {
            x.SetButton(errors.Count == 0);
        });
    }

    public Hex GetHexOfType(Hex.HexType type)
    {
        var query = from a in spawned.Values
                    where a.Type == type
                    select a;

        if (query.Count() != 1) return null;

        return query.ElementAt(0);
    }

    void Start ()
    {
        // Calculating offset size
        RecalculateOffsets();

        // Loading map
        try
        {
            LoadHexmapFromFile(mapName);
        }
        catch (Exception e)
        {
            // Failed to load from file!
            Debug.Log("Failed to load map from file " + mapName + ": " + e.Message);
            LoadHexmap();
        }
    }

    void Update()
    {
        if (transform.hasChanged)
        {
            // If map spawned - change positions for all map nodes
            if (spawned != null)
            {
                spawned.ToList().ForEach(
                    kvp => kvp.Value.GetComponent<Transform>().SetPositionAndRotation(
                            GetHexOffset((int)kvp.Key.col, (int)kvp.Key.row),
                            Quaternion.identity
                        ));
            }

            transform.hasChanged = false;
        }
    }

    public void UpdateHexProps()
    {
        hexes.Clear();
        spawned.Values.ToList().ForEach(x => hexes.Add(x.GenerateProps()));
    }

    /*!
     * \brief Loading map
     */
    public void LoadHexmap(int rows = 10, int cols = 10)
    {
        // TODO: specify exact dimentions of hexmap

        // Null check for extra safety
        if (hexes == null) hexes = new List<HexProps>();
        if (spawned == null) spawned = new Dictionary<Coordinates.Hexagonal, Hex>();

        // Check if map is already loaded
        if ((hexes.Count > 0) || (spawned.Count > 0))
        {
            // TODO: display warning?
            hexes.Clear();
            spawned.Values.ToList().ForEach(x => Destroy(x.gameObject));
            spawned.Clear();
        }

        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
            {
                HexProps p = new HexProps();
                p.x = i;
                p.y = j;
                p.typeEnum = Hex.HexType.PLANE;
                hexes.Add(p);
            }

        // Spawning hexes
        SpawnLoadedMap();
        UpdateMapStatus();

        Debug.Log("Map instantiation complete. Loaded " + hexes.Count + " nodes, spawned: " + spawned.Count);
    }

    public void LoadHexmapFromFile(GameObject textObject)
    {
        string text = textObject.GetComponent<Text>().text;
        if ((text != null) && (text.Length > 0)) LoadHexmapFromFile(text);
    }

    /*!
     * \brief Loading map from .yml file
     */
    public void LoadHexmapFromFile(string filename)
    {
        // TODO: !remove duplicate code

        // Null check for extra safety
        if (hexes == null) hexes = new List<HexProps>();
        if (spawned == null) spawned = new Dictionary<Coordinates.Hexagonal, Hex>();

        // Check if map is already loaded
        if ((hexes.Count > 0) || (spawned.Count > 0))
        {
            // TODO: display warning?
            hexes.Clear();
            spawned.Values.ToList().ForEach(x => Destroy(x.gameObject));
            spawned.Clear();
        }

        hexes = HexGridLoader.LoadHexNodes("Assets/Maps/" + filename + ".yml");

        // Spawning hexes
        SpawnLoadedMap();
        UpdateMapStatus();

        Debug.Log("Map instantiation complete. Loaded " + hexes.Count + " nodes, spawned: " + spawned.Count);
    }

    // TODO: add UI check for empty field
    public void SaveLoadedMap(GameObject textObject)
    {
        string text = textObject.GetComponent<Text>().text;
        if ((text != null) && (text.Length > 0)) SaveLoadedMap(text);
    }

    /*!
     * \brief Saves current loaded map to the file
     */
    public void SaveLoadedMap(string name)
    {
        // Check if any hexes has been loaded
        if (hexes == null)
        {
            // TODO: throw exception?
            return; // map not loaded
        }

        // Update props
        UpdateHexProps();

        // Call save function
        HexGridLoader.DumpToFile("Assets/Maps/" + name + ".yml", hexes);
    }

    /*!
     * \brief Spawning loaded hexes. if hexes are not loaded - throws error.
     */
    private void SpawnLoadedMap()
    {
        // Check if any hexes has been loaded
        if (hexes == null)
        {
            // TODO: throw exception?
            return; // map not loaded
        }

        // Initialize new map dictionary / Delete previous map data
        if (spawned == null) spawned = new Dictionary<Coordinates.Hexagonal, Hex>();
        else spawned.Clear();
        
        hexes.ForEach(x => SpawnHex(x));
    }

    /*!
     * \brief Handles spawning of individual hexes.
     */
    private void SpawnHex(HexProps parameters)
    {
        GameObject hexObject = Instantiate(hexPrefab, GetHexOffset(parameters.x, parameters.y), Quaternion.identity);

        Hex h = hexObject.GetComponent<Hex>();

        h.Parent = this;
        h.Coords = new Coordinates.Hexagonal(parameters.x, parameters.y);
        h.Type = parameters.typeEnum;

        spawned.ToList().ForEach(kvp => CheckAdjacent(h, kvp.Value));
        spawned.ToList().ForEach(kvp => CheckDiagonal(h, kvp.Value));

        spawned.Add(h.Coords, h);
    }

    /*!
     * \brief Sets appropriate variables for adjacent hexes.
     */
    private void CheckAdjacent(Hex a, Hex b)
    {
        if (a.IsNeighbor(b))
        {
            a.SetNeighbor(b, a.GetNeighborDirection(b));
            b.SetNeighbor(a, b.GetNeighborDirection(a));
        }
    }

    private void CheckDiagonal(Hex a, Hex b)
    {
        if (a.IsDiagonal(b))
        {
            a.SetDiagonal(b, a.GetDiagonalDirection(b));
            b.SetDiagonal(a, b.GetDiagonalDirection(a));
        }
    }

    /*!
     * \brief Calculating X and Y offsets based on radius.
     */
    private void RecalculateOffsets()
    {
        // Calculating offsets
        offsetX = radius * Mathf.Sqrt(3);
        offsetY = radius * 1.5f;
    }

    /*!
     * \brief Calculates 3D offsets for hex with specified coordinates.
     */
    private Vector3 GetHexOffset(int x, int z)
    {
        // “odd-q” vertical layout
        // TODO: odd-r
        Vector3 position = Vector3.zero;

        if (z % 2 == 0)
        {
            position.x = x * offsetX;
            position.z = - z * offsetY;
        }
        else
        {
            position.x = (x + 0.5f) * offsetX;
            position.z = - z * offsetY;
        }

        // Adjust to actual map position
        position.x += gameObject.transform.position.x;
        position.z += gameObject.transform.position.z;

        return position;
    }

}
