﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexSpawner : MonoBehaviour
{
    /*
    private float NearestHexDistance = 99999f;
    private GameObject NearestHexObject;

    private void CalculateDistanseToNearestHex(GameObject other)
    {
        float dist = Vector3.Distance(other.transform.position, transform.position);
        if (NearestHexDistance > dist)
        {
            // if (NearestHexObject) NearestHexObject.GetComponent<Hex>().UnhighlightHex();

            NearestHexDistance = dist;
            NearestHexObject = other;

            // other.GetComponent<Hex>().HighlightHex();
        }
    }
    */

    private bool IsHex(GameObject o)
    {
        Hex temp = o.GetComponent<Hex>();

        return temp;
    }

    private void FixedUpdate()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 1,5);

        Collider nearestCollider = null;
        float minSqrDistance = Mathf.Infinity;

        for (int i = 0; i < colliders.Length; i++)
        {
            if (!IsHex(colliders[i].gameObject)) continue;

            float sqrDistanceToCenter = Vector3.Distance(transform.position, colliders[i].transform.position);

            if (sqrDistanceToCenter < minSqrDistance)
            {
                minSqrDistance = sqrDistanceToCenter;
                nearestCollider = colliders[i];
            }
        }

        if (nearestCollider)
        {
            // TODO: FindNearestSide
            // TODO: StickToSide
            // nearestCollider.gameObject.GetComponent<Hex>().HighlightHex();
        }
    }
}
