﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using UnityEngine.EventSystems;

/*!
 * \brief Represents single hex object.
 */
public class Hex : MonoBehaviour
{
    public BuildableObject structure;

    public enum HexType { NONE, PLANE, OBSTRUCTED, SPAWNER, PATH, HEART };
    private HexType type;
    public HexType Type
    {
        get { return type; }
        set
        {
            // Take note on Spawners and Hearts
            if (type == HexType.HEART) Parent.CountOfHearts--;
            if (type == HexType.SPAWNER) Parent.CountOfSpawners--;

            // Assign type value
            type = value;

            // Change renderer material
            Renderer rend = GetComponent<Renderer>();
            rend.material = TypeMaterials[(int)Type];

            // Take note on Spawners and Hearts
            if (value == HexType.HEART) Parent.CountOfHearts++;
            if (value == HexType.SPAWNER) Parent.CountOfSpawners++;

            if (Parent) Parent.UpdateMapStatus();
        }
    }
    public Material[] TypeMaterials;

    public HexGrid Parent;

    private Coordinates.Hexagonal coords;
    public Coordinates.Hexagonal Coords
    {
        get { return coords; }
        set { coords = value; }
    }

    public HexProps GenerateProps()
    {
        HexProps props = new HexProps();
        props.x = coords.col;
        props.y = coords.row;
        props.type = (int)Type;
        return props;
    }

    private Dictionary<GridMovement.Directions, Hex> neighbors;

    public Hex GetHeighbor(GridMovement.Directions d)
    {
        return neighbors[d];
    }

    public void SetNeighbor(Hex hex, GridMovement.Directions d)
    {
        neighbors[d] = hex;
    }

    private Dictionary<GridMovement.Diagonals, Hex> diagonals;

    public Hex GetDiagonal(GridMovement.Diagonals d)
    {
        return diagonals[d];
    }

    public void SetDiagonal(Hex hex, GridMovement.Diagonals d)
    {
        diagonals[d] = hex;
    }

    void Awake ()
    {
        // Initialize neighbor and diagonal dictionaries
        neighbors = new Dictionary<GridMovement.Directions, Hex>(6);
        diagonals = new Dictionary<GridMovement.Diagonals, Hex>(6);

        // Set default type
        Type = HexType.PLANE;
    }

    private void OnMouseUpAsButton()
    {
        if (EventSystem.current.IsPointerOverGameObject()) return;

        HexType currentHexPlacing = GameObject.FindGameObjectWithTag("GameController").GetComponent<MainController>().HexPlacing;

        if (currentHexPlacing != HexType.NONE) Type = currentHexPlacing;
    }

    /*!
     * \brief Checks if selected Hex has path to Heart.
     * \returns Error message of NULL if all good.
     */
    public string CheckPath()
    {
        try
        {
            FindPath();
        }
        catch (Exception e)
        {
            switch (e.Message)
            {
                case "Branching path":
                    return "No branching paths allowed";
                case "Dead end":
                    return "Path is not connected to the Heart";
                default:
                    return "Error while finding path";
            }
        }
        return null;
    }

    public List<Hex> FindPath()
    {
        return FindPath(null);
    }

    private List<Hex> FindPath(Hex previous)
    {
        // Check if current Hex is Heart
        if (Type == HexType.HEART)
        {
            // Return itself in a List
            return new List<Hex>() { this };
        }

        // Find next Hex
        List<Hex> possiblePath = FindNeighorsByType(new List<HexType> { HexType.HEART, HexType.PATH });
        if (previous != null) possiblePath.Remove(previous);

        // Check for branching paths
        if (possiblePath.Count > 1)
            throw new Exception("Branching path");

        // Check for dead end
        if (possiblePath.Count == 0)
            throw new Exception("Dead end");

        // Recursion
        List<Hex> result = possiblePath[0].FindPath(this);

        // Add itself and return
        result.Add(this);
        return result;
    }

    // NEIGHBORS

    /*!
     * \brief Checks if specified hex is adjacent to this hex.
     */
    public bool IsNeighbor(Hex other)
    {
        // Determine offset
        Coordinates.Hexagonal offset = other.coords - coords;

        // Determine parity
        int parity = Coordinates.GetParity(Coords);

        // Check if parity array contains calculated offset
        return Coordinates.Oddr_NDirections[parity].Contains(offset);
    }

    public GridMovement.Directions GetNeighborDirection(Hex other)
    {
        //if (!IsNeighbor(other)) throw new Exception("Specified hex is not an assigned neighbor"); // TODO: rework exception*/
        if (neighbors.ContainsValue(other))
            return neighbors.FirstOrDefault(x => x.Value.Coords.Equals(other.Coords)).Key;
        else
            return (GridMovement.Directions)Array.IndexOf(
                Coordinates.Oddr_NDirections[Coordinates.GetParity(Coords)],
                other.coords - coords
                );
    }

    public List<Hex> FindNeighorsByType(HexType t)
    {
        return FindNeighorsByType(new List<HexType> { t });
    }

    public List<Hex> FindNeighorsByType(List<HexType> t)
    {
        List<Hex> result = new List<Hex>();

        neighbors.Values.ToList().ForEach(x =>
        {
            if (t.Contains(x.Type)) result.Add(x);
        });

        return result;
    }

    // DIAGONALS

    /*!
     * \brief Checks if specified hex is diagonal to this hex.
     */
    public bool IsDiagonal(Hex other)
    {
        // Determine offset
        Coordinates.Hexagonal offset = other.coords - coords;

        // Determine parity
        int parity = Coordinates.GetParity(Coords);

        // Check if parity array contains calculated offset
        return Coordinates.Oddq_DDirections[parity].Contains(offset);
    }

    public GridMovement.Diagonals GetDiagonalDirection(Hex other)
    {
        if (neighbors.ContainsValue(other))
            return diagonals.FirstOrDefault(x => x.Value.Coords.Equals(other.Coords)).Key;
        else
            return (GridMovement.Diagonals)Array.IndexOf(
                Coordinates.Oddq_DDirections[Coordinates.GetParity(Coords)],
                other.coords - coords
                );
    }
    
    public override string ToString()
    {
        // TODO: add more info
        StringBuilder s = new StringBuilder("[Hex ");
        s.Append(coords.col); s.Append(","); s.Append(coords.row); s.Append("]");
        return s.ToString();
    }

    private void OnDestroy()
    {
        // Take note on Spawners and Hearts
        if (type == HexType.HEART) Parent.CountOfHearts--;
        if (type == HexType.SPAWNER) Parent.CountOfSpawners--;
        Parent.UpdateMapStatus();
    }

}
