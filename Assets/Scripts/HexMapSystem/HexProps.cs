﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexProps
{
    public int x { get; set; }
    public int y { get; set; }
    public Hex.HexType typeEnum;
    public int type
    {
        set
        {
            typeEnum = (Hex.HexType)value;
        }
        get
        {
            return (int)typeEnum;
        }
    }
}
