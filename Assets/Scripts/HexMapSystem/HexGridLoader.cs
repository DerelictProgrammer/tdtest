﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using YamlDotNet.RepresentationModel;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using System.Linq;
using System.Text;

/*!
 * \brief Handles map loading process from .yml files
 */
public static class HexGridLoader {

    /*!
     * \brief Loads hex nodes from YML file. 
     */
	public static List<HexProps> LoadHexNodes(string filename)
    {
        // Get fill file name
        string fullpath = Path.GetFullPath(filename);

        // Check file existance
        if (!FileExists(fullpath))
            throw new FileNotFoundException(fullpath);

        // Check file extension
        if (!IsYml(fullpath))
            throw new FormatException("Specified file is not YAML file");

        // Create YamlStream from StringReader for file data
        var stream = new YamlStream();
        stream.Load(new StringReader(ReadFromFile(filename)));

        // Get first document from stream
        var document = stream.Documents.First();

        // Get SequenceNode for tile information
        YamlSequenceNode rootSequence = (YamlSequenceNode)document.RootNode;

        // Parse tile data to resulting list
        List<HexProps> result = new List<HexProps>();
        rootSequence.ToList().ForEach(x => result.Add(ProcessNode((YamlMappingNode)x)));

        return result;
    }

    private static HexProps ProcessNode(YamlMappingNode node)
    {
        // Get tile properties dictionary
        var dict = node.Children.ToDictionary(t => t.Key.ToString(), t => t.Value.ToString());

        HexProps result = new HexProps();
        result.x = int.Parse(dict["x"]);
        result.y = int.Parse(dict["y"]);
        result.typeEnum = (Hex.HexType)int.Parse(dict["type"]);
        return result;
    }

    /*!
     * \brief Checks if specified file is YAML file by extension.
     */
    public static bool IsYml(string filename)
    {
        string ext = Path.GetExtension(filename);
        return ext == ".yml" || ext == ".yaml";
    }

    /*!
     * \brief Checks if specified file exists by trying to read it.
     */
    public static bool FileExists(string filename)
    {
        try
        {
            // Trying to open a file stream
            FileStream tmp = File.OpenRead(filename);
            tmp.Close();
            return true;
        }
        catch { return false; }
    }

    /*!
     * \brief Reads text from file via StreamReader into String.
     */
    public static string ReadFromFile(string fileName)
    {
        // Initialize result as empty string.
        StringBuilder result = new StringBuilder();

        try
        {   // Open the text file using a stream reader and read data into the StringBuilder.
            using (StreamReader sr = new StreamReader(fileName))
                result.Append(sr.ReadToEnd());
        }
        catch (Exception e)
        {
            Console.WriteLine("The file could not be read:");
            Console.WriteLine(e.Message);
            return "";
        }

        return result.ToString();
    }

    public static void DumpToFile(string filename, List<HexProps> map)
    {
        File.Delete(filename);
        using (TextWriter writer = File.CreateText(filename))
        {
            Serializer serializer = new Serializer(SerializationOptions.EmitDefaults);

            serializer.Serialize(writer, map);
        }
        
    }
}
