﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartButton : MonoBehaviour
{
    public void SetButton(bool state)
    {
        GetComponent<Button>().enabled = state;
    }
}
