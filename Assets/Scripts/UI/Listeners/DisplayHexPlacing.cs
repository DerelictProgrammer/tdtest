﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayHexPlacing : MonoBehaviour
{

    private void Awake()
    {
        gameObject.GetComponent<Text>().text = "Use buttons above to select type of tile";
    }

    public void Display(string hexType)
    {
        // TODO: change to actual Enum value check
        if (hexType.Equals("NONE"))
        {
            gameObject.GetComponent<Text>().text = "Use buttons above to select tile type";
            return;
        }

        gameObject.GetComponent<Text>().text = "Tile type selected: " + hexType + ". Click on any tile to change it. ESC to cancel.";
    }
}
