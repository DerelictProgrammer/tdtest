﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayNameOfSelectedTile : MonoBehaviour
{
    void Awake()
    {
        EventManager.OnObjectSelected += Display;
    }

    private void Display(GameObject o)
    {
        if (!o.GetComponent<Hex>()) return;

        gameObject.GetComponent<Text>().text = "Tile: " + o.GetComponent<Hex>().Type.ToString();
    }
}
