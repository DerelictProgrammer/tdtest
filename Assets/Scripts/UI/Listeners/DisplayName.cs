﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayName : MonoBehaviour
{
    void Awake()
    {
        EventManager.OnObjectSelected += Display;
    }

    private void Display(GameObject o)
    {
        if (!o.GetComponent<NameAndDescription>()) return;

        gameObject.GetComponent<Text>().text = o.GetComponent<NameAndDescription>().Name;
    }
}
