﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MapErrors : MonoBehaviour
{

    public List<string> errorMessages;

    public void DisplayErrors()
    {
        Text text = GetComponent<Text>();

        if (errorMessages.Count == 0)
        {
            text.text = "Ready to play!";
        }
        else
        {
            text.text = "";
            errorMessages.ForEach(x => text.text += x + '\n');
        }
    }
}
