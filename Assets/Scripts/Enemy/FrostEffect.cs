﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostEffect : StatusEffect
{
    private Creep creep;
    public float SlowAmount = 0.2f;

    public override void ActivateEffect()
    {
        Creep c = Subject.gameObject.GetComponent<Creep>();
        if (!c) return;

        c.SpeedModifier -= SlowAmount;
        creep = c;
    }

    public override void DeactivateEffect()
    {
        creep.SpeedModifier += SlowAmount;
    }
}
