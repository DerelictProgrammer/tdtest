﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using System.Linq;
using System;
using UnityEngine;

public class CreepSpawner : MonoBehaviour
{

    public float InitialDelay = 3;
    public float WaveDelay = 10;

    public Timer t;

    private MainController controller;
    private float timeToTimer;
    public int CurrentWave = 0;

    private void Awake()
    {
        // Set controller
        controller = GameObject.FindObjectOfType<MainController>();

        // Set timer
        t = new Timer();
        t.Elapsed += (sender, e) => WaveTimerElapsed();
        timeToTimer = Time.time;

        // Activate timer
        ResetTimer(InitialDelay);
        Debug.Log("Timer started");
    }

    private void Update()
    {
        controller.UpdateWaveTimer(Math.Ceiling(Math.Max(timeToTimer - Time.time, 0)));
        controller.UpdateWaveCounter(CurrentWave);
    }

    private void WaveTimerElapsed()
    {
        CurrentWave++;
        ResetTimer(WaveDelay);
        controller.SpawnNewWave();
    }

    private void ResetTimer(float timeInSeconds)
    {
        // Set time to timer
        timeToTimer += timeInSeconds;
        
        t.Interval = timeInSeconds * 1000;
        t.Start();
    }
}
