﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*!
 * \brief Handles Creep behaviour.
 * \remark Baking navmesh at run-time or implementing individual
 *         A* is excessive. The simplest possible pathfinding and
 *         movement methods will be used.
 */
public class Creep : Killable
{
    private Hex PreviousHex;
    private Hex CurrentHex;
    public float Speed = 1f;
    public float SpeedModifier = 1;

    /*!
     * \brief Initializes instantiated Creep for it to have proper pathfinding.
     */
    public void Initialize(Hex spawner)
    {
        // Check to prevent double initialization
        if (!CurrentHex)
        {
            CurrentHex = spawner;
            PreviousHex = null;
        }
    }

    private void FixedUpdate()
    {
        // Check if we there yet
        if ((CurrentHex.transform.position - transform.position).magnitude > 0.4f)
        {
            // Move creep
            Move();
        }
        else
        {
            if (CurrentHex.Type == Hex.HexType.HEART)
            {
                GameObject.FindObjectOfType<MainController>().GetHit();
                Destroy(gameObject);
                return;
            }
            FindNextHex();
        }
    }

    protected virtual void Move()
    {
        transform.position = Vector3.MoveTowards(
            transform.position,
            new Vector3(CurrentHex.transform.position.x, 0.3f, CurrentHex.transform.position.z),
            (Speed * SpeedModifier) * Time.deltaTime); // TODO: move that to GetSpeed method
    }

    protected virtual void FindNextHex()
    {
        List<Hex> directions = CurrentHex.FindNeighorsByType(new List<Hex.HexType> { Hex.HexType.PATH, Hex.HexType.HEART });
        directions.Remove(PreviousHex);
        if (directions.Count != 1)
            throw new Exception("Path not found");
        PreviousHex = CurrentHex;
        CurrentHex = directions[0];
    }
}
