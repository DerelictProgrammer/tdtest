﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Killable : MonoBehaviour
{
    public float BaseHP;
    public int GpValue;
    public int PointsValue;
    public List<StatusEffect> StatusEffects;

    // Damage reduction
    public float DR = 0;

    private const float WAVE_HP_MODIFIER = 0.2f;

    private void Awake()
    {
        CreepSpawner spawner = GameObject.FindObjectOfType<CreepSpawner>();
        StatusEffects = new List<StatusEffect>();

        // Spawner may be already despawned (ha-ha)
        if (!spawner) Destroy(gameObject);
        else BaseHP *= spawner.CurrentWave * WAVE_HP_MODIFIER;
    }

    private void Update()
    {
        StatusEffects.ForEach(s =>
        {
            if (Time.time > s.ActiveUntil)
            {
                s.DeactivateEffect();
                StatusEffects.Remove(s);
            }
        });
    }

    public void TakeDamage(float value)
    {
        value -= DR;

        if (BaseHP > value) BaseHP -= value;
        else Kill();
    }

    public void Kill()
    {
        MainController controller = GameObject.FindObjectOfType<MainController>();
        controller.GP += GpValue;
        controller.Score += PointsValue;

        Destroy(gameObject);
    }

    /*!
     * \brief Activates StatusEffect upon this object.
     */
    public void InflictStatusEffect(StatusEffect effect)
    {
        // Check if similar effect is active on current object.
        bool contains = false;
        StatusEffects.Where(s => s != null).Where(s => s.Type.Equals(effect.Type))
            .ToList().ForEach(s => 
            {
                // Found similar effect, updating timer
                // TODO: update other conditions, like Slow, DPS, etc
                contains = true;
                s.ResetTime();
            });

        if (!contains)
        {
            StatusEffects.Add(effect);
            effect.Subject = this;
            effect.ResetTime();
            effect.ActivateEffect();
        }
    }

    public void RemoveStatusEffect(StatusEffect effect)
    {
        StatusEffects.Remove(effect);
    }
}
