﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusEffect
{
    // TODO: this can be Enum
    public string Type;
    public float Duration;
    public Killable Subject;
    public float ActiveUntil;

    public void ResetTime()
    {
        ResetTime(Duration);
    }

    public void ResetTime(float newDuration)
    {
        if (Time.time + newDuration > ActiveUntil) ActiveUntil = Time.time + newDuration;
    }

    public virtual void ActivateEffect() { }

    public virtual void DeactivateEffect() { }
}
