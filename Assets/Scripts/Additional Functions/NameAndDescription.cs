﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameAndDescription : MonoBehaviour
{
    public string Name;
    public string Description;
}
