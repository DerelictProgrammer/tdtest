﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectableObject : MonoBehaviour
{
    // It is better to die for the Emperor than live for yourself

    // TODO: make it activate on DRAG as well
    private void OnMouseUpAsButton()
    {
        // TODO: Block UI-hitting raycasts
        if (EventSystem.current.IsPointerOverGameObject()) return;

        Globals.LastSelectedObject = gameObject;
    }

    /*!
     * \brief Stub method, called when current object is selected
     */
    public virtual void Highlight() { }

    /*!
     * \brief Stub method, called when current object is deselected
     */
    public virtual void Unhighlight() { }
}
