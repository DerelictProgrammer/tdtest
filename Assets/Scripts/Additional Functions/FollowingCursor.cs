﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingCursor : MonoBehaviour
{
    public bool FollowingCursorActive;
    RaycastHit hit;
    Ray ray;
    int layerMask = 1 << 8;

    protected virtual void Update()
    {
        if (FollowingCursorActive)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {
                Vector3 temp = hit.point;
                temp.y = 1;
                transform.position = temp;
            }
        }
    }
}
