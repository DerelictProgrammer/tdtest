﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*!
 * \brief Serves as a static hub for all events - Mediator pattern
 * \warning This is appropriate only in small apps. Breakup in more complex systems to avoid God Object
 */
public class EventManager : MonoBehaviour 
{
    // On selecting object
    public delegate void ObjectSelected(GameObject o);
    public static event ObjectSelected OnObjectSelected;

    public static void ObjectSelectedTrigger(GameObject o)
    {
        // TODO: Upgrade to C# v6.* to use null propagating operator: OnObjectSelected?.Invoke(o);
        if (OnObjectSelected != null) OnObjectSelected(o);
    }
}
