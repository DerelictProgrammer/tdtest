﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{
    // TODO: fix this mess
    public Hex.HexType HexPlacing { get; private set; }
    public GameObject TimerText;
    public GameObject WaveText;
    public GameObject ScoreText;
    public GameObject GPText;
    public GameObject EndGameCanvas;
    public float CreepDelay;

    private bool NewWave = false;
    private Hex Spawner;
    public int CreepCount;
    public GameObject[] Creeps;

    private int score = 0;
    [HideInInspector]
    public int Score
    {
        set
        {
            score = value;
            ScoreText.GetComponent<Text>().text = "Score: " + score;
        }
        get
        {
            return score;
        }
    }
    private int gp = 0;
    [HideInInspector]
    public int GP
    {
        set
        {
            gp = value;
            GPText.GetComponent<Text>().text = "GP: " + gp;
        }
        get
        {
            return gp;
        }
    }

    public GameObject LivesText;
    public int InitialLives;
    private int LivesLeft;

    /*!
     * \brief That is a stub for a future function of creating new Hexes. 
     */
    public void SetHexPlacing(string htype)
    {
        // TODO: handle NULLs somehow
        HexPlacing = (Hex.HexType)System.Enum.Parse(typeof(Hex.HexType), htype.ToUpper());
        FindObjectsOfType<DisplayHexPlacing>().ToList().ForEach(x => x.Display(htype));
    }

    /*!
     * \brief Catches ESC key. Checks for new waves.
     */
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            ResetHexlacing();

        if (NewWave)
        {
            StartCoroutine(SpawnWave());

            NewWave = false;
        }
    }

    /*!
     * \brief Sets flag for a new Creep Wave.
     */
    public void SpawnNewWave()
    {
        NewWave = true;
    }

    /*!
     * \brief Spawns <CreepCount> Creeps with <CreepDelay> pause between Creeps
     */
    private IEnumerator SpawnWave()
    {
        int count = CreepCount;
        int creepId = Random.Range(0, Creeps.Length) % Creeps.Length;

        for (int i = 0; i < count; i++)
        { 
            // Instantiate creep
            GameObject creep = Instantiate(Creeps[creepId], new Vector3(Spawner.gameObject.transform.position.x, 0.3f, Spawner.gameObject.transform.position.z), Quaternion.identity);
            creep.GetComponent<Creep>().Initialize(Spawner);

            // Wait for next creep
            yield return new WaitForSeconds(CreepDelay);
        }
    }

    /*!
     * \brief Resets state of HexPlacing process. Unpins all appropriate buttons
     */
    public void ResetHexlacing()
    {
        HexPlacing = Hex.HexType.NONE;
        FindObjectsOfType<DisplayHexPlacing>().ToList().ForEach(x => x.Display(Hex.HexType.NONE.ToString()));
    }

    /*!
     * \brief Resets state of HexPlacing process. Unpins all appropriate buttons
     */
    public void StartGame()
    {
        // Resetting HexPlacing
        ResetHexlacing();

        // Adding CreepSpawner script to the Spawner Hex.
        GameObject spawner = FindObjectOfType<HexGrid>().GetHexOfType(Hex.HexType.SPAWNER).gameObject;
        spawner.AddComponent<CreepSpawner>();
        Spawner = spawner.GetComponent<Hex>();

        // TODO: find adequate way to dynamicly assign price values to text labels above build buttons
        // Setting variables
        LivesLeft = InitialLives;
        Score = 0;
        GP = 100;

        LivesText.GetComponent<Text>().text = "Lives left: " + LivesLeft;
    }

    /*!
     * \brief Called when Creep reaches Heart. Decreases Player Life by 1.
     */
    public void GetHit()
    {
        LivesLeft--;
        LivesText.GetComponent<Text>().text = "Lives left: " + LivesLeft;

        if (LivesLeft <= 0) EndGame();
    }

    /*!
     * \brief Suspends Game Mode.
     */
    public void EndGame()
    {
        // Stop creep spawning
        CreepSpawner spawnerScript = Spawner.gameObject.GetComponent<CreepSpawner>();
        spawnerScript.t.Stop();

        // Remove spawners
        Destroy(spawnerScript);

        // Destroy all creeps and towers
        GameObject.FindGameObjectsWithTag("Enemy").ToList().ForEach(x => Destroy(x));
        GameObject.FindGameObjectsWithTag("Tower").ToList().ForEach(x => Destroy(x));

        // Show popup
        var enumerator = EndGameCanvas.transform.GetChild(0).GetEnumerator();
        while (enumerator.MoveNext())
        {
            Transform child = (Transform)enumerator.Current;
            if (!child.name.Equals("txt_endgame")) continue;

            child.GetComponent<Text>().text = "Your score: " + Score;
        }
            EndGameCanvas.GetComponent<Hideable>().Show();
    }

    /*!
     * \brief Spends GP if possible.
     */
    public bool TrySpendGold(int value)
    {
        if (value > GP) return false;

        GP -= value;
        return true;
    }
    
    /*!
     * \brief Updates text message.
     */
    public void UpdateWaveTimer(double value)
    {
        TimerText.GetComponent<Text>().text = value.ToString("F0") + "s to next wave";
    }

    /*!
     * \brief Updates text message.
     */
    public void UpdateWaveCounter(int value)
    {
        if (value > 0) WaveText.GetComponent<Text>().text = "Wave " + value.ToString();
        else WaveText.GetComponent<Text>().text = "Get ready!";
    }

    /*!
     * \brief Starts Build mode.
     */
    public void StartBuilding(GameObject tower)
    {
        Instantiate(tower, new Vector3(), Quaternion.identity);
    }
}
