﻿using System;
using System.IO;
using System.Collections.Generic;
using YamlDotNet.RepresentationModel;

public class YMLParser {

    public static string ReadFromFile(string fileName)
    {
        string result = "";
        try
        {   // Open the text file using a stream reader.
            using (StreamReader sr = new StreamReader(fileName))
            {
                result += sr.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("The file could not be read:");
            Console.WriteLine(e.Message);
            return "";
        }
        return result;
    }

    /*
    public static IDictionary<string, AICEDescriptor> GetAICEDescriptors()
    {
        IDictionary<string, AICEDescriptor> result = new Dictionary<string, AICEDescriptor>();

        var input = new StringReader(ReadFromFile("Assets\\Conf\\Characters.yml"));

        var yaml = new YamlStream();
        yaml.Load(input);

        // Examine the stream
        var mapping =
            (YamlMappingNode)yaml.Documents[0].RootNode;

        var items = (YamlSequenceNode)mapping.Children[new YamlScalarNode("characters")];

        // Iterate over characters
        int id = 0;
        foreach (YamlMappingNode item in items)
        {
            IList<Need> needs = new List<Need>();
            var needList = (YamlSequenceNode)item.Children[new YamlScalarNode("needs")];
            foreach (YamlScalarNode need in needList)
            {
                // TODO: fetch need
                // needs.Add(new Need(Type, rate, init));
            }
            AICEDescriptor d = new AICEDescriptor(id, item.Children[new YamlScalarNode("name")]);
            id++;
        }

        return result;
    }*/
}
