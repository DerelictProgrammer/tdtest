﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Coordinates {

    public class Hexagonal 
    {
        public int row, col;

        public Hexagonal()
        {
            row = 0; col = 0;
        }

        public Hexagonal(int col, int row)
        {
            this.row = row; this.col = col;
        }

        public static Hexagonal operator +(Hexagonal left, Hexagonal right)
        {
            return new Hexagonal(left.col + right.col, left.row = right.row);
        }

        public static Hexagonal operator -(Hexagonal right)
        {
            return new Hexagonal(-right.col, - right.row);
        }

        public static Hexagonal operator -(Hexagonal left, Hexagonal right)
        {
            return new Hexagonal(left.col - right.col, left.row - right.row);
        }

        public override bool Equals(object other)
        {
            if (other == null) return false;

            var item = other as Hexagonal;
            return (item.col == col) && (item.row == row);
        }

        public override int GetHashCode()
        {
            // I dont think there will be maps more than 9999 hexes long...
            // TODO: add \warning
            return col.GetHashCode() * 1000 + row.GetHashCode();
        }

    }

    public class Cubical
    {
        public int x, y, z;

        public Cubical()
        {
            x = 0; y = 0; z = 0;
        }

        public Cubical(int x, int y, int z)
        {
            this.x = x; this.y = y; this.z = z;
        }
    }

    public static Hexagonal[][] Oddr_NDirections = new Hexagonal[][]
        {
            new Hexagonal[] // parity 0 (EVEN row: 0, 2, 4...)
            {
                new Hexagonal( 0, -1), // NW
                new Hexagonal(+1,  0), // N
                new Hexagonal( 0, +1), // NE
                new Hexagonal(-1, +1), // SE
                new Hexagonal(-1,  0), // S
                new Hexagonal(-1, -1)  // SW
            },
            new Hexagonal[] // parity 1 (ODD row: 1, 3, 5...)
            {
                new Hexagonal(+1, -1), // NW
                new Hexagonal(+1,  0), // N
                new Hexagonal(+1, +1), // NE
                new Hexagonal( 0, +1), // SE
                new Hexagonal(-1,  0), // S
                new Hexagonal( 0, -1)  // SW
            }
        };

    public static Hexagonal[][] Oddq_DDirections = new Hexagonal[][]
        {
            new Hexagonal[] // parity 1 (ODD row: 1, 3, 5...)
            {
                new Hexagonal(-2, +1), // SW
                new Hexagonal( 0, +2), // W
                new Hexagonal(+1, +1), // NW
                new Hexagonal(+1, -1), // NE
                new Hexagonal( 0, -2), // E
                new Hexagonal(-2, -1)  // SE
            },
            new Hexagonal[] // parity 0 (EVEN row: 0, 2, 4...)
            {
                new Hexagonal(-1, +1), // SW
                new Hexagonal( 0, +2), // W
                new Hexagonal(+2, +1), // NW
                new Hexagonal(+2, -1), // NE
                new Hexagonal( 0, -2), // E
                new Hexagonal(-1, -1)  // SE
            }
        };

    public static int GetParity(Hexagonal coords)
    {
        return coords.row % 2;
    }

    // TODO: “odd-r” vertical layout
    /*!
     * \brief Converts Cubical coordinates to Oddq-hexagonal format.
     */
    public static Hexagonal CubeToOddq(Cubical cube)
    {
        int col = cube.x;
        int row = cube.z + (cube.x - (cube.x & 1)) / 2;
        return new Hexagonal(col, row);
    }

    /*!
     * \brief Converts Oddq-hexagonal coordinates to Cubical format.
     */
    public static Cubical OddqToCube(Hexagonal hex)
    {
        int x = hex.col;
        int z = hex.row - (hex.col - (hex.col & 1)) / 2;
        int y = -x - z;
        return new Cubical(x, y, z);
    }
}
