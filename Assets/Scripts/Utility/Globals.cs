﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals : MonoBehaviour
{
    private static GameObject lastSelectedObject;
    public static GameObject LastSelectedObject
    {
        set
        {
            if (lastSelectedObject != null) lastSelectedObject.GetComponent<SelectableObject>().Unhighlight();
            lastSelectedObject = value;
            value.GetComponent<SelectableObject>().Highlight();
            EventManager.ObjectSelectedTrigger(value);
        }
        get
        {
            return lastSelectedObject;
        }
    }


}
