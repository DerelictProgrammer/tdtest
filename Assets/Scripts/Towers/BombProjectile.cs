﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BombProjectile : Projectile
{
    protected Vector3 TargetPos;
    public float ExplosionRadius;

    protected override void OnTriggerEnter(Collider other)
    { }

    public override void SetTarget(GameObject t)
    {
        TargetPos = t.gameObject.transform.position;
    }

    protected override void Update()
    {
        if (transform.position.Equals(TargetPos))
        {
            // Explosion!
            Explode();
            Destroy(gameObject);
        }
        else transform.position = Vector3.MoveTowards(
                transform.position,
                TargetPos,
                Speed * Time.deltaTime);
        // else Destroy(gameObject);
    }

    protected virtual void Explode()
    {
        Physics.OverlapSphere(transform.position, ExplosionRadius, LayerMask.GetMask("Enemy")).ToList().ForEach(e => HitTarget(e.gameObject));
    }
}
