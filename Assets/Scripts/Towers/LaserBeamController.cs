﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBeamController : MonoBehaviour
{
    public GameObject Origin = null;
    public GameObject Destination = null;

    private LineRenderer line;

    public void Awake()
    {
        line = GetComponent<LineRenderer>();
        line.positionCount = 2;
        line.startWidth = 0.1f;
        line.endWidth = 0.1f;
    }

    void Update ()
    {
        // TODO: that means beam will instantly dissapear when killing creep. Creep should stay "dead" for a while, /then/ be destroyed.
        if ((Destination == null) || (!Destination))
        {
            line.enabled = false;
            return;
        }
        
        Vector3 beamOrigin = gameObject.transform.GetChild(0).position;
        Vector3 heading = Destination.transform.position - beamOrigin;
        Ray ray = new Ray(beamOrigin, heading / heading.magnitude);
        RaycastHit hit;


        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            line.enabled = true;
            line.SetPosition(0, Origin.transform.position);
            line.SetPosition(1, Destination.transform.position);
        }
    }
}
