﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tower : MonoBehaviour
{
    public float FireDelay = 1;

    protected bool Firing = false;
    protected float LastFired = 0;
    protected Creep Target;

    public void SetFiring(bool f)
    {
        Firing = f;
    }

    private void Update()
    {
        if (CheckTarget())
        {
            // Target acuired, trying to open fire
            if (Time.time - LastFired > FireDelay) Fire();
        }
        else SearchForTarget();
    }

    public virtual void Fire() { }

    public virtual void SearchForTarget() { }

    public virtual bool CheckTarget() { return false; }
}
