﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostProjectile : Projectile
{
    protected override void HitTarget(GameObject t)
    {
        base.HitTarget(t);
        Target.gameObject.GetComponent<Killable>().InflictStatusEffect(new FrostEffect()
        {
            Type = "Frostbite",
            Duration = 3,
            SlowAmount = 0.5f
        });
    }
}
