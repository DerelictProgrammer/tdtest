﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombTower : ProjectileTower
{
    public float SplashRadius;

    public override void Fire()
    {
        if ((!Firing) || (!Target) || (Time.time - LastFired < ShotsDelay)) return;

        LastFired = Time.time;
        // TODO: projectile instantiation should be in separate function
        BombProjectile p = Instantiate(Projectile, ProjectileOrigin.transform.position, Quaternion.identity).GetComponent<BombProjectile>();
        p.SetTarget(Target.gameObject);
        p.Damage = Damage;
        p.Speed = Speed;
        p.ExplosionRadius = SplashRadius;
    }
}
