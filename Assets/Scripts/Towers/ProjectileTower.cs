﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileTower : Tower
{
    public int FireRadius;
    public float ShotsDelay;
    public float Speed;
    public float Damage;
    public GameObject Projectile;
    public GameObject ProjectileOrigin;

    public override void Fire()
    {
        if ((!Firing) || (!Target) || (Time.time - LastFired < ShotsDelay)) return;
        
        LastFired = Time.time;
        // TODO: add projectile rotation to face the target
        Projectile p = Instantiate(Projectile, ProjectileOrigin.transform.position, Quaternion.identity).GetComponent<Projectile>();
        p.SetTarget(Target.gameObject);
        p.Damage = Damage;
        p.Speed = Speed;
    }

    // TODO: set targeting alg to get closest creep to the Heart?
    public override void SearchForTarget()
    {
        Collider[] enemies = Physics.OverlapSphere(transform.position, FireRadius, LayerMask.GetMask("Enemy"));

        if (enemies.Length == 0) Target = null;
        else Target = enemies[0].gameObject.GetComponent<Creep>();
    }

    public override bool CheckTarget()
    {
        if (!Target) return false;

        if (Vector3.Distance(gameObject.transform.position, Target.gameObject.transform.position) > FireRadius)
        {
            Target = null;
            return false;
        }

        return true;
    }
}
