﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    protected GameObject Target;
    [HideInInspector]
    public float Speed;
    [HideInInspector]
    public float Damage;

    protected virtual void Update()
    {
        if (Target)
            transform.position = Vector3.MoveTowards(
                transform.position,
                Target.transform.position,
                Speed * Time.deltaTime);
        else Destroy(gameObject);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Target)
        {
            HitTarget(Target);
            Destroy(gameObject);
        }
    }

    protected virtual void HitTarget(GameObject t)
    {
        t.GetComponent<Creep>().TakeDamage(Damage);
    }

    public virtual void SetTarget(GameObject t)
    {
        Target = t;
    }
}
