﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NormalTower : Tower
{
    public int FireRadius;
    public float ShotsDelay;
    public float Damage;

    public void Awake()
    {
        var enumerator = transform.GetEnumerator();
        while (enumerator.MoveNext())
        {
            Transform child = (Transform)enumerator.Current;
            if (!child.name.Equals("Ray Origin")) continue;

            // TODO: handle situation where no Ray Origin child present
            GetComponent<LaserBeamController>().Origin = child.gameObject;
        }
    }

    public override void Fire()
    {
        if ((!Firing) || (!Target) || (Time.time - LastFired < ShotsDelay)) return;

        LastFired = Time.time;
        StartCoroutine(Beam());

        Target.TakeDamage(Damage);
    }

    private IEnumerator Beam()
    {
        LaserBeamController laser = GetComponent<LaserBeamController>();
        laser.Destination = Target.gameObject;
        
        yield return new WaitForSeconds(0.1f);

        laser.Destination = null;
    }

    // TODO: set targeting alg to get closest creep to the Heart?
    public override void SearchForTarget()
    {
        Collider[] enemies = Physics.OverlapSphere(transform.position, FireRadius, LayerMask.GetMask("Enemy"));

        if (enemies.Length == 0) Target = null;
        else Target = enemies[0].gameObject.GetComponent<Creep>();
    }

    public override bool CheckTarget()
    {
        if (!Target) return false;

        if (Vector3.Distance(gameObject.transform.position, Target.gameObject.transform.position) > FireRadius)
        {
            Target = null;
            return false;
        }

        return true;
    }
}
