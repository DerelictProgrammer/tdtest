﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildableObject : FollowingCursor
{
    private bool BuildingInProgress;
    public int Price;

    private void Awake()
    {
        FollowingCursorActive = true;
        BuildingInProgress = true;
    }

    protected override void Update()
    {
        base.Update();
        if ((BuildingInProgress) && (Input.GetKeyDown(KeyCode.Escape)))
            Destroy(gameObject);
    }

    private void OnMouseUpAsButton()
    {
        if (!BuildingInProgress) return;

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 8))
        {
            // Find object hit
            Hex h = hit.transform.gameObject.GetComponent<Hex>();

            // Check if it's really Hex
            if (!h) return;

            if ((h.Type.Equals(Hex.HexType.PLANE)) && (!h.structure))
            {
                // Build tower
                if (!GameObject.FindObjectOfType<MainController>().TrySpendGold(Price))
                {
                    // TODO: say 'Not enough gold'
                    return;
                }

                FollowingCursorActive = false;
                BuildingInProgress = false;
                transform.position = new Vector3(h.transform.position.x, 1, h.transform.position.z);
                h.structure = this;
                gameObject.GetComponent<Tower>().SetFiring(true);
            }
            else
            {
                // TODO: Cannot build
            }
        }
    }
}
